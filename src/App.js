import React from 'react'
import ClassCounter from './components/ClassCounter'
import ClassCounter5 from './components/ClassCounter5'
import ClassCounter6 from './components/ClassCounter6'
import HookCounter3 from './components/HookCounter3'
import HookForm4 from './components/HookForm4'
import HooksCounter from './components/HooksCounter'
import HooksCounter2 from './components/HooksCounter2'
import HooksCounter5 from './components/HooksCounter5'
import HooksCounter6 from './components/HooksCounter6'

export default function App() {
  return (
    <div>
      {/* <ClassCounter /> */}
      {/* <HooksCounter /> */}
      {/* <HooksCounter2 /> */}
      {/* <HookCounter3 /> */}
      {/* <HookForm4 /> */}
      {/* <ClassCounter5 /> */}
      {/* <HooksCounter5 /> */}
      {/* <ClassCounter6 /> */}
      <HooksCounter6 />
    </div>
  )
}
