import React, { useState } from 'react'

const HookCounter3 = () => {

    const [items, setItems] = useState ([]);

    const addItems = () => {
        setItems([...items, {
            id: items.length,
            value: Math.floor(Math.random() * 10) + 1
        }])
    }

  return (
    <div>
        <button onClick={addItems}>Add Items</button>

        {items.map((key) => (
            <div key={key.id}>
                {key.value}
            </div>
        ))}
    </div>
  )
}

export default HookCounter3