import React, { useState } from 'react'

const HookForm4 = () => {

    const [name, setName] = useState ({firstName: '', lastName: ''});
    const changeName = (event) => {
        setName({...name, [event.target.name]: event.target.value})
    }
  return (
    <div>
        <form>
            <input type="text" defaultValue={name.firstName} onChange={changeName} name="firstName"/>
            <input type="text" defaultValue={name.lastName} onChange={changeName} name="lastName"/>
            <h1>{JSON.stringify(name)}</h1>
        </form>
    </div>
  )
}

export default HookForm4