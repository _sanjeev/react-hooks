import React, { useState } from 'react'

const HooksCounter2 = () => {
    const initialValue = 0;
    const [count, setCount] = useState (initialValue);

    const incrementByFive = () => {
        for (let index = 0; index < 5; index++) {
            setCount((prevCount) => {
                return prevCount + 1;
            });
        }
    }

  return (
    <div>
        count: {count}
        <button onClick={() => {setCount(count + 1)}}>Increment</button>
        <button onClick={() => {setCount(count - 1)}}>Decrement</button>
        <button onClick={() => {setCount(initialValue)}}>Reset</button>
        <button onClick={incrementByFive}>Increment by 5</button>
    </div>
  )
}

export default HooksCounter2