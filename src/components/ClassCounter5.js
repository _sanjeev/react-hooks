import React, { Component } from 'react'

export default class ClassCounter5 extends Component {
    constructor(props) {
        super(props);
        this.state = {
            count: 0,
        }
    }
    componentDidMount = () => {
        document.title = `You Clicked ${this.state.count} times`
    }

    componentDidUpdate = () => {
        document.title = `You CLicked ${this.state.count} times`
    }

    handleClick = () => {
        this.setState({
            count: this.state.count + 1,
        })
    }

  render() {
    return (
      <div>
          <button onClick={this.handleClick}>Clicked {this.state.count} times</button>
      </div>
    )
  }
}
