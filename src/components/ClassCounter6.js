import React, { Component } from "react";

export default class ClassCounter6 extends Component {
  constructor(props) {
    super(props);
    this.state = {
      count: 0,
      name: ''
    };
  }

  componentDidMount = () => {
    document.title = `Clicked ${this.state.count} times`;
  };

  componentDidUpdate = (prevProps, prevState) => {
    if (prevState.count !== this.state.count) {
        console.log('componentDidUpdate');
        document.title = `Clicked ${this.state.count} times`;
    }
  };

  render() {
    return (
      <div>
        <input
          type="text"
          name="name"
          onChange={(e) => {
            this.setState({
              [e.target.name]: e.target.value,
            });
          }}
        />
        <button
          onClick={() => {
            this.setState({
              count: this.state.count + 1,
            });
          }}
        >
          Clicked {this.state.count} times
        </button>
      </div>
    );
  }
}
