import React, { useEffect, useState } from "react";

const HooksCounter6 = () => {
    const [count, setCount] = useState(0);
    const [name, setName] = useState('');

    useEffect (() => {
        console.log('Component Update');
        document.title = `Clicked ${count} times`
    }, [name][count])

    // useEffect (() => {
    //     console.log('Component Update');
    // }, [count])

  return (
      <div>
          <input type="text" name='name' onChange={(e) => {
              setName (e.target.value);
          }} />
          <button onClick={() => {
              setCount(count + 1);
          }}>Count {count}</button>
      </div>
  )
};

export default HooksCounter6;
